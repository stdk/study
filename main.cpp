#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int main() {

    DIR *dir;
    struct dirent * entry;

    struct stat stbuf;

    dir = opendir ("./");
    if (dir == NULL)
    {
        fprintf (stderr, "Ошибка открытия директории\n");
        return(1);
    }

    char *errormessage, *divider;
    char buffer[100] = {0};

    struct passwd *p;
      uid_t  uid=0;

    while ((entry = readdir (dir)) != NULL)
    {
     int ret;
     if (!(ret = stat(entry->d_name, &stbuf)))
     {

        //printf("%-8d ", stbuf.st_uid);
        printf("%u\t", stbuf.st_nlink);

        uid=stbuf.st_uid;
        if (p = getpwuid(uid)) {
          printf("getpwuid() error");
        } else {
          printf("%s\t",p->pw_name);
        }

        printf("%jd\t%lo\t", (intmax_t)stbuf.st_size,stbuf.st_mode);

        //strftime(buffer,sizeof(buffer),"%b", localtime(&stbuf.st_mtime));

        //printf("%s\t", buffer);
        strftime(buffer,sizeof(buffer),"%b  %d %H:%M", localtime(&stbuf.st_mtime));
        printf("%s ", buffer);
        printf("%s\t\n", entry->d_name);
      }
      else
        {
          printf("%s error code: %i\n", entry->d_name, ret);
        }
    }

    closedir(dir);
    return(0);
}


